class Question < ApplicationRecord
	extend FriendlyId
	has_many :answers
	validates :title, presence: true, length: { maximum: 150 }
	validates :body, presence: true, length: { in: 50..1500 }

	friendly_id :title, use: [:slugged, :history]

	def should_generate_new_friendly_id?
		!has_friendly_id_slug? || title_changed?
	end

	def has_friendly_id_slug?
		slugs.where(slug: slug).exists?
	end
end
