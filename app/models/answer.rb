class Answer < ApplicationRecord
  belongs_to :question, dependent: :destroy
  belongs_to :user

  validates :body, presence: true, length: { maximum: 2500 }
end
