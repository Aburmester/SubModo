class AnswersController < ApplicationController
	before_action :find_question
	before_action :authenticate_user!


	def new 
		@answer = Answer.new
		@meta_title = meta_title("Svar")
	end

	def create
		@answer = @question.answers.new(answer_params)

		if @answer.save
			redirect_to :back, notice: "Svar indsendt!"
		else
			redirect_to :back, notice: "Dit svar blev ikke sendt."
		end
	end

	def destroy
	end


	private

		def answer_params
			params.require(:answer).permit(:body).merge(user: current_user)
		end

		def find_question
			@question = Question.friendly.find(params[:question_id]) 
		end
end
